FROM alpine:edge

RUN apk --no-cache --repository http://dl-4.alpinelinux.org/alpine/edge/testing add \
    texlive-full \
    && wget http://ftp.math.utah.edu/pub/tex/historic/systems/texlive/2016/texlive-20160523b-texmf.tar.xz \
    && tar -xJf texlive-20160523b-texmf.tar.xz \
    && cp -r texlive-20160523-texmf/texmf-dist /usr/share \
    && rm -rf texlive* \
    && apk fix texlive \
    && ln -s /usr/bin/mktexlsr /usr/bin/mktexlsr.pl
